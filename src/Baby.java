
import java.math.BigInteger;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author limbo
 */
public class Baby {
   public void run_BSGS(BigInteger y, BigInteger g, BigInteger pEl){

 long start = System.currentTimeMillis();
 //inisialisasi nilai N
 BigInteger N =(sqrt(pEl.subtract(BigInteger.ONE))).add(BigInteger.ONE);
 //syarat nilai N kuadrat lebih dari p-1
 while(true){
 if((N.pow(2)).compareTo(pEl.subtract(BigInteger.ONE)) > 0)
 break;
 else
 N = N.add(BigInteger.ONE);}
       ArrayList<BigInteger> baby_list = new ArrayList<>();
       ArrayList<BigInteger> giant_list = new ArrayList<>();
       ArrayList<BigInteger> hasil = new ArrayList<>();
 while(true){
 System.out.println("Nilai N = "+N);
 for(BigInteger j = BigInteger.ZERO; j.compareTo(N) < 0; j =
j.add(BigInteger.ONE)){
 //baby step
 BigInteger value = g.modPow(j, pEl);
 baby_list.add(value);
 System.out.println("Index ke-"+j+" baby step = "+value);
 //giant step
 BigInteger g_inv = g.modInverse(pEl);
 BigInteger value2 = (g_inv.modPow(N.multiply(j),
pEl)).multiply(y).mod(pEl);
 giant_list.add(value2);
 System.out.println("Index ke-"+j+" giant step = "+value2); }
 System.out.println("\n");
 //mencari nilai yang sama antara baby list dan giant list
 for(int i=0; i<baby_list.size(); i++){
 for(int j=0; j<giant_list.size(); j++){
 if(baby_list.get(i).equals(giant_list.get(j))){
 BigInteger result =
 BigInteger.valueOf(i).add(BigInteger.valueOf(j).multiply(N));
 hasil.add(result); } } }
 if(!hasil.isEmpty())
 break;
 else //meningkatkan nilai jika tidak ada yang sama
 N = N.add(BigInteger.ONE); }
long  waktu_bsgs = System.currentTimeMillis()-start; } 

    private Object sqrt(BigInteger subtract) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
