

import static java.lang.Math.E;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

public class Srnn {
 String plaintext;
 public BigInteger p, q, n, e, d, u, a, u_a,b,v,v_e;
 public BigInteger phi_n;
 BigInteger kurang;
 int panjang_bit = 32;
 int panjang_bit2 =8000;
 String cipher = "";
 String plain = "";
public void keyGeneration(int panjang_bit){
 b = new BigInteger(panjang_bit2, new Random());
 long start = System.currentTimeMillis();
 //BigInteger p, q, n, e, d;
 while(true){
 
 //System.out.println("Nilai B: " + b);
 p = new BigInteger(panjang_bit, 10, new Random());
 System.out.println("Nilai P: " + p);
 q = new BigInteger(panjang_bit, 10, new Random());
 System.out.println("Nilai q: " + q);
 if(!p.equals(q))
break;
}
 n = p.multiply(q);
 System.out.println("Nilai N: " + n);
 phi_n =(p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
 System.out.println("Nilai Phi: " + phi_n);
 while(true){
 e = new BigInteger(panjang_bit-24, new Random());
//rumus(2.3)
 if(e.gcd(phi_n).equals(BigInteger.ONE))
 break;
}
  System.out.println("Nilai e: " + e);
  d = e.modInverse(phi_n); //rumus (2.4)
  System.out.println("Nilai d: " + d);
  u = new BigInteger("2");
  System.out.println("Nilai u :"+ u);
  //BigInteger s = phi_n.subtract(BigInteger.ONE);
  a = u.add(BigInteger.ONE);
  System.out.println("Nilai a :"+ a);
  u_a = u.modPow(a,b);
  System.out.println("Nilai u^a :" +u_a);
  long end = System.currentTimeMillis();
  long waktu = end-start;
  System.out.println("waktu pembangkitan : "+ String.valueOf(waktu)+"milisecond");
}
 
public void enkripsi(String plaintext, BigInteger e, BigInteger n, BigInteger u_a){
 long start2 = System.currentTimeMillis(); 
 for(int i=0; i<plaintext.length(); i++){
 int M = (int)plaintext.charAt(i);
 BigInteger Q = BigInteger.valueOf(M).multiply(u_a);
 //System.out.println("nilai Mess: "+Q);
 BigInteger C = Q.modPow(e, n); //enkripsi
 cipher = cipher+C+" ";
 }
 //long waktu_enkripsi = System.currentTimeMillis()-start2;
 
    System.out.println("Hasil Enkripsi :"+cipher);
    long end2 = System.currentTimeMillis();
    long waktu2 = end2-start2;
    System.out.println("waktu enkripsi : "+ String.valueOf(waktu2)+"milisecond");
 }
 public void dekripsi(String ciphertext){
     //System.out.println("Cipher : "+ciphertext);
 long start4 = System.currentTimeMillis();
     //System.out.println("nilai a : "+a);
     //System.out.println("nilai phi : "+phi_n);
 kurang = phi_n.subtract(a);
     //System.out.println("phi-a :"+kurang);
 v = u.modPow(kurang, n);
     //System.out.println("nilai v :"+v);
 v_e = v.modPow(e, b) ;
     //System.out.println("nilai v^e:"+v_e);
     String data_cipher[] = ciphertext.split(" ");
     System.out.println("sampai sini");    
 for(int i=0; i<data_cipher.length; i++){
 BigInteger X = new BigInteger(data_cipher[i]).multiply(v_e);
     //System.out.println("Hasil X :"+X);
 BigInteger M = X.modPow(d, n);
     //System.out.println("Hasil M :"+M);
 byte ASCII = Byte.parseByte(M.toString());
 plain = plain+(char)ASCII;
 }
    System.out.println("Hasil Dekripsi : "+plain);
    long end4 = System.currentTimeMillis();
    long waktu4 = end4-start4;
    System.out.println("waktu dekripsi : "+ String.valueOf(waktu4)+"milisecond");
 }
  public static void main(String[] args) 
    {
        
        Srnn demo=new Srnn();
        Scanner saya = new Scanner(System.in);
        //String teststring;
        System.out.println("Enter the plain text:");
        String plain = saya.nextLine();
        demo.keyGeneration(demo.panjang_bit);
        demo.enkripsi(plain, demo.e,demo.n,demo.u_a);
        demo.dekripsi(demo.cipher);
        
        
        
       
    }

 } 

     
