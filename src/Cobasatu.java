

import static java.lang.Math.E;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

public class Cobasatu {

 
 String plaintext;
 public BigInteger p, q, n, e, d;
 int panjang_bit = 16;
 String cipher = "";
 String plain = "";
public void keyGeneration(int panjang_bit){
 long start = System.currentTimeMillis();
 //BigInteger p, q, n, e, d;
 while(true){
 p = new BigInteger(panjang_bit, 10, new Random());
 System.out.println("Nilai P: " + p);
 q = new BigInteger(panjang_bit, 10, new Random());
 System.out.println("Nilai q: " + q);
 if(!p.equals(q))
break;
}
 n = p.multiply(q);
 System.out.println("Nilai N: " + n);
 BigInteger phi_n =(p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
 System.out.println("Nilai Phi: " + phi_n);
 while(true){
 e = new BigInteger(panjang_bit-2, new Random());
//rumus(2.3)
 if(e.gcd(phi_n).equals(BigInteger.ONE))
 break;
}
  System.out.println("Nilai e: " + e);
  d = e.modInverse(phi_n); //rumus (2.4)
  System.out.println("Nilai d: " + d);
  long end = System.currentTimeMillis();
  long waktu = end-start;
  System.out.println("waktu pembangkitan : "+ String.valueOf(waktu)+"milisecond");
}
 
public void enkripsi(String plaintext, BigInteger e, BigInteger n){
 long start2 = System.currentTimeMillis();
 
 for(int i=0; i<plaintext.length(); i++){
 int M = (int)plaintext.charAt(i);
 //System.out.println(" "+M);
 BigInteger C = BigInteger.valueOf(M).modPow(e, n); 
 cipher = cipher+C+" ";
 }
 //long waktu_enkripsi = System.currentTimeMillis()-start2;
 
    System.out.println(cipher);
    long end2 = System.currentTimeMillis();
    long waktu2 = end2-start2;
    System.out.println("waktu enkripsi : "+ String.valueOf(waktu2)+"milisecond");
 }
 public void dekripsi(String ciphertext){
 long start4 = System.currentTimeMillis();

 String data_cipher[] = ciphertext.split(" ");
 for(int i=0; i<data_cipher.length; i++){
 BigInteger M = (new BigInteger(data_cipher[i])).modPow(d, n);
 byte ASCII = Byte.parseByte(M.toString());
 plain = plain+(char)ASCII;
 }
     System.out.println(plain);
     long end4 = System.currentTimeMillis();
    long waktu4 = end4-start4;
    System.out.println("waktu dekripsi : "+ String.valueOf(waktu4)+"milisecond");
 }
  public static void main(String[] args) 
    {
        
        
        Cobasatu demo=new Cobasatu();
        Scanner saya = new Scanner(System.in);
        //String teststring;
        System.out.println("Enter the plain text:");
        String plain = saya.nextLine();
        demo.keyGeneration(demo.panjang_bit);
        demo.enkripsi(plain, demo.e,demo.n);
        demo.dekripsi(demo.cipher);
        
        
        
       
    }

 } 

     
