/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aye;

/**
 *
 * @author limbo
 */
import java.io.DataInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;
 
public class Aye
{
    private BigInteger p;
    private BigInteger q;
    private BigInteger N;
    private BigInteger phi;
    private BigInteger e;
    private BigInteger d;
    private BigInteger u;
    private BigInteger a;
    private int        bitlength =1024 ;
    private Random     r;
 
    public Aye()
    {
        r = new Random();
        p = BigInteger.probablePrime(bitlength, r);
        System.out.println("Nilai P: " + p);
        q = BigInteger.probablePrime(bitlength, r);
        System.out.println("Nilai q: " + q);
        N = p.multiply(q);
        System.out.println("Nilai p.q: " + N);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        System.out.println("Nilai phi: " + phi);
        e = BigInteger.probablePrime(bitlength / 2, r);
        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0)
        {
            e.add(BigInteger.ONE);
        }
        System.out.println("Nilai e: " + e);
        d = e.modInverse(phi);
        System.out.println("Nilai d: " + d);
        u = BigInteger.probablePrime(bitlength / 4, r);
        System.out.println("Nilai u:" +u);
       // a = BigInteger.(Math.random() * u) +r ;
        
    }
 
    public Aye(BigInteger e, BigInteger d, BigInteger N)
    {
        this.e = e;
        this.d = d;
        this.N = N;
    }
 
    
    public static void main(String[] args) 
    {
        Aye rsa = new Aye();
        Scanner saya = new Scanner(System.in);
        //String teststring;
        System.out.println("Enter the plain text:");
        String teststring = saya.nextLine();
        System.out.println("Encrypting String: " + teststring);
        System.out.println("String in Bytes: "
                + bytesToString(teststring.getBytes()));
        // encrypt
        byte[] encrypted = rsa.encrypt(teststring.getBytes());
        // decrypt
        byte[] decrypted = rsa.decrypt(encrypted);
        System.out.println("Decrypting Bytes: " + bytesToString(decrypted));
        System.out.println("Decrypted String: " + new String(decrypted));
    }
 
    private static String bytesToString(byte[] encrypted)
    {
        String test = "";
        for (byte b : encrypted)
        {
            test += Byte.toString(b);
        }
        return test;
    }
 
    // Encrypt message
    public byte[] encrypt(byte[] message)
    {
        return (new BigInteger(message)).modPow(e, N).toByteArray();
    }
 
    // Decrypt message
    public byte[] decrypt(byte[] message)
    {
        return (new BigInteger(message)).modPow(d, N).toByteArray();
    }
}
