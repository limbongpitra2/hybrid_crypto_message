


import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

public class Elsrnn {
    BigInteger pEl;
    BigInteger x;
    BigInteger g,p,q,n,e,phi_n,d,b,a,a_inv;
    BigInteger y;
    int panjang_bit=32;
    String cipher = "";
    String cipher2 = "";
    String plain = "";
public void keyGeneration(int panjang_bit){

 long start = System.currentTimeMillis();

while(true){
 p = new BigInteger(panjang_bit, 10, new Random());
 q = new BigInteger(panjang_bit, 10, new Random());
 if(!p.equals(q))
 break;
 }
 n = p.multiply(q); //rumus (2.10)
  phi_n =(p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
//rumus (2.11)
 while(true){
e = new BigInteger(panjang_bit-16, new Random());
 if(e.gcd(phi_n).equals(BigInteger.ONE))
 break;
}
 while(true){
 pEl = new BigInteger(panjang_bit, 10, new Random());
 if(pEl.compareTo(e) > 0)
 break;
}
 //rumus (2.12)
 while(true){
 x = new BigInteger(panjang_bit-2, new Random());
 g = new BigInteger(panjang_bit-2, new Random());
 
 if(!x.equals(g))
 if(!x.equals(e) && !x.equals(d))
 if(!g.equals(e) && !g.equals(d))
 break;
 }
 d = e.modInverse(phi_n);
 y = g.modPow(x, pEl); //rumus (2.13)
 long waktu_pembangkitan_kunci = System.currentTimeMillis()-start;
 } 
 

public void enkripsi(String plaintext, BigInteger pEL, BigInteger G,BigInteger Y, BigInteger e, BigInteger n){

long start = System.currentTimeMillis();
BigInteger k = new BigInteger(pEL.bitLength()-5, new Random());
System.out.println("Nilai k : "+k);
 a = G.modPow(k, pEL);
cipher2 = cipher2+a+" ";
cipher = cipher+a+" ";
for(int i=0; i<plaintext.length(); i++){
int M = (int)plaintext.charAt(i);
System.out.println(M);
BigInteger b = Y.modPow(k, pEL).multiply(BigInteger.valueOf(M)).mod(pEL); //rumus (2.8)
cipher2 = cipher2+b+" ";
BigInteger C = b.modPow(e, n);
cipher = cipher+C+" "; 
}
System.out.println("Nilai enkripsi pertama : "+cipher2);
System.out.println("Nilai enkripsi kedua   : "+cipher);
float waktu_enkripsi = System.currentTimeMillis()-start;
    System.out.println("Waktu enkripsi: "+waktu_enkripsi+"MS");

}
 public void dekripsi(String ciphertext){
 long start = System.currentTimeMillis();
  String plain2 =" ";
 String data_cipher[] = ciphertext.split(" ");
  //a = (new BigInteger(data_cipher[0])).modPow(d, n);
  a_inv =a.modPow(pEl.subtract(BigInteger.ONE).subtract(x),pEl);
 for(int i=1; i<data_cipher.length; i++){
 b = (new BigInteger(data_cipher[i])).modPow(d, n);
     System.out.println(b);
     BigInteger M = b.multiply(a_inv).mod(pEl); //rumus (2.16)
     System.out.println("nilai m : "+M);
    
    byte ASCII = Byte.parseByte(M.toString());
    plain2 = plain2+(char)ASCII;
}
     System.out.println("plaintext :"+plain2);
     long waktu_dekripsi = System.currentTimeMillis()-start;

 }  
 
    public static void main(String[] args) {
        Elrsa baru=new Elrsa();
        Scanner saya = new Scanner(System.in);
        String teststring;
        System.out.println("Enter plaintext:");
        String plain2 = saya.nextLine();
        baru.keyGeneration(baru.panjang_bit);
        baru.enkripsi(plain2, baru.pEl, baru.g, baru.y,baru.e, baru.n);
        baru.dekripsi(baru.cipher);
        
    }
}
