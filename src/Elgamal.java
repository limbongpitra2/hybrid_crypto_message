
import java.math.BigInteger;
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author limbo
 */
public class Elgamal {
    BigInteger pEl;
    BigInteger x;
    BigInteger g;
    BigInteger y;
    int panjang_bit=8;
    String cipher = "";
    String plain = "";
public void keyGeneration(int panjang_bit){
 float start4 = System.currentTimeMillis();
 pEl = new BigInteger(panjang_bit, 10, new Random());
 System.out.println(pEl);
 while(true){
 x = new BigInteger(panjang_bit-2, new Random());
     System.out.println(x);
 g = new BigInteger(panjang_bit-2, new Random());
 System.out.println(g);
 if(!x.equals(g))
 break;
 }
 y = g.modPow(x, pEl); //rumus (2.7)
 System.out.println(y);
 //long waktu_pembangkitan_kunci = System.currentTimeMillis()-start;
    float end4 = System.currentTimeMillis();
    float waktu4 = end4-start4;
    System.out.println("waktu pembangkitan kunci : "+ waktu4+"milisecond");
}
public void enkripsi(String plaintext, BigInteger PEL, BigInteger G,BigInteger Y){

long start = System.currentTimeMillis();
BigInteger k = new BigInteger(PEL.bitLength()-5, new Random());
    System.out.println("Nilai k : "+k);
BigInteger a = G.modPow(k, PEL);
//System.out.println("Nilai a : "+a);
cipher = cipher+a+" ";
for(int i=0; i<plaintext.length(); i++){
int M = (int)plaintext.charAt(i);
    System.out.println(M);
BigInteger b = Y.modPow(k, PEL).multiply(BigInteger.valueOf(M)).mod(PEL); //rumus (2.8)
cipher = cipher+b+" "; 
}
System.out.println(cipher);
float waktu_enkripsi = System.currentTimeMillis()-start;
    System.out.println("Waktu enkripsi: "+waktu_enkripsi+"MS");

}
public void dekripsi(String ciphertext){
long start = System.currentTimeMillis();
String data_cipher[] = ciphertext.split(" ");
BigInteger a_inv = (new BigInteger(data_cipher[0])).modPow(pEl.subtract(BigInteger.ONE).subtract(x), pEl); 
for(int i=1; i<data_cipher.length; i++){
BigInteger M = (new BigInteger(data_cipher[i])).multiply(a_inv).mod(pEl); //rumus (2.8)
byte ASCII = Byte.parseByte(M.toString());
plain = plain+(char)ASCII;
}
long waktu_dekripsi = System.currentTimeMillis()-start;
System.out.println(plain);
System.out.println("waktu dekripsi :"+waktu_dekripsi+"MS");
 } 
    public static void main(String[] args) {
        Elgamal baru=new Elgamal();
        Scanner saya = new Scanner(System.in);
        String teststring;
        System.out.println("Enter plaintext:");
        String plain2 = saya.nextLine();
        baru.keyGeneration(baru.panjang_bit);
        baru.enkripsi(plain2, baru.pEl, baru.g, baru.y);
        baru.dekripsi(baru.cipher);
        
    }
}
